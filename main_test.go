package main

import (
	"testing"
	"reflect"
)

func TestAdd(t *testing.T) {
	t.Run("Add 1 and 2 ", func (t *testing.T) {
		got := add(1,2)
		want := 3
		if ! reflect.DeepEqual(got,want) {
			t.Error("Want: ", want, "but got: ", got)
		}
	})

	t.Run("Add 3 and 4 ", func (t *testing.T) {
		got := add(3,4)
		want := 7
		if ! reflect.DeepEqual(got,want) {
			t.Error("Want: ", want, "but got: ", got)
		}
	})
}
